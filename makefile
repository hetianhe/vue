build:
	docker build -t savadenn/vue .
test: build
	@docker rm -f test_vue || true
	docker run --rm -it \
		--name test_vue \
		-v $(shell pwd):/app \
    	-u "$(shell id -u):$(shell id -g)" \
    	-w "/app" \
    	--net=host \
    	savadenn/vue vue ui
