# Vue

Docker image with [Vue/cli](https://cli.vuejs.org/) installed.

## Usage

```console
docker run --rm -it        \
    --name test_vue        \
    -v $(pwd):/app         \
    -u "$(id -u):$(id -g)" \
    -w "/app"              \
    --net=host             \
    savadenn/vue vue ui
```
